from django.db import models
from django.shortcuts import render
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.core import hooks
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from menu.models import get_nonempty_menu_list, get_menu_list
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, FieldRowPanel, InlinePanel
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtail.contrib.forms.models import AbstractFormField, AbstractEmailForm
from wagtail.core.fields import RichTextField


@hooks.register('register_rich_text_features')
def register_blockquote_feature(features):
    features.default_features.append('h1')


class HomePage(Page):
    restaurant_name = models.CharField(max_length=255, default="New Restaurant")
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        related_name='+',
        default=1
    )
    header_text = models.CharField(max_length=255, default='Welcome to our restaurant!')
    awards = RichTextField(default="We've won a lot of awards")
    menu_header = RichTextField(default="Discover our best dishes!")
    about_us = RichTextField(default="We are an Italian Restaurant with history")

    content_panels = Page.content_panels + [
        FieldPanel('restaurant_name'),
        ImageChooserPanel('header_image'),
        FieldPanel('header_text'),
        FieldPanel('awards'),
        FieldPanel('menu_header'),
        FieldPanel('about_us')
    ]

    template = 'home.html'
    subpage_types = ['home.FormPage', 'menu.MenuIndex']

    class Meta:
        verbose_name = 'homepage'

    def serve(self, request, *args, **kwargs):
        context = self.get_context(self, request, *args, **kwargs)
        context['menu_list_notempty'] = get_nonempty_menu_list()
        key_filter = request.GET.get('filter')
        if key_filter:
            context['menu_list'] = get_menu_list(key_filter)
        else:
            context['menu_list'] = get_nonempty_menu_list()
        return render(request, self.template, context)


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE, related_name='form_fields')


class FormPage(AbstractEmailForm):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('thank_you_text', classname="full"),
        FormSubmissionsPanel(),
    ]

    template = 'bug-form.html'
