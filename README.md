# Setup instructions

### Note: if you use Windows or MacOS you have to omit first step and download python from https://www.python.org/downloads/ the newest release of python may also install as "python3" instead of "python3.6" so use "python3" in command line instead of "python3.6" ###
### You also have to download git from: https://git-scm.com/downloads ###
Make sure you have installed all needed libraries and dependencies (run these commands in command line):
sudo apt-get update
sudo apt-get install python3.6 git

Clone the repository to your computer using this command in terminal:
git clone https://kmularski@bitbucket.org/kmularski/restaurant-app.git

In terminal change working directory to directory with app and create virtual env:
cd restaurant-app/
python3.6 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

If you want to use fixtures instead of sample 'db.sqlite3' database you can create new one with with these commands:
python3.6 manage.py makemigrations
python3.6 manage.py migrate
python3.6 manage.py loaddata

Create a superuser:
python3.6 manage.py createsuperuser

Collect static files:
python3.6 manage.py collectstatic

To run virtual server:
python3.6 manage.py runserver


Now you can access the app through web browser with url: 127.0.0.1:8000
