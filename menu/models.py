from django.db import models
from django.shortcuts import render
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel


def get_nonempty_menu_list():
    all_menus = Menu.objects.all().live()
    filtered_menus = []
    for menu in all_menus:
        if menu.get_first_child():
            menu = {'id': menu.id, 'title': menu.title, 'dishes_no': menu.get_children_count(), 'menu': menu}
            filtered_menus.append(menu)
    return filtered_menus


def get_menu_list(key_filter):
    filtered_menus = get_nonempty_menu_list()
    return sorted(filtered_menus, key=lambda k: k[key_filter])


class MenuIndex(Page):
    description = RichTextField(default="List of all menus")
    content_panels = Page.content_panels + [
        FieldPanel('description')
    ]

    template = 'menu-index.html'
    subpage_types = ['menu.Menu']

    class Meta:
        verbose_name = "menu index"

    def serve(self, request, *args, **kwargs):
        context = self.get_context(self, request, *args, **kwargs)
        context['menu_list_notempty'] = get_nonempty_menu_list()
        key_filter = request.GET.get('filter')
        if key_filter:
            context['menu_list'] = get_menu_list(key_filter)
        else:
            context['menu_list'] = get_nonempty_menu_list()
        return render(request, self.template, context)


class Menu(Page):
    description = RichTextField(default="A very tasty menu")
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+',
        default=1
    )
    content_panels = Page.content_panels + [
        FieldPanel('description'),
        ImageChooserPanel('header_image')
    ]

    template = 'menu.html'
    subpage_types = ['menu.Dish']

    class Meta:
        verbose_name = "menu"
        verbose_name_plural = "menus"

    def get_context(self, request, *args, **kwargs):
        context = super(Menu, self).get_context(self, request, *args, **kwargs)
        context['dishes'] = Dish.objects.descendant_of(self).live()
        context['menu_list_notempty'] = get_nonempty_menu_list()
        return context


class Dish(Page):
    description = RichTextField(default='Tasty dish')
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+',
        default=1
    )

    content_panels = Page.content_panels + [
        FieldPanel('description'),
        ImageChooserPanel('header_image'),
    ]

    template = 'dish.html'

    class Meta:
        verbose_name = "dish"
        verbose_name_plural = "dishes"

    def get_context(self, request, *args, **kwargs):
        context = super(Dish, self).get_context(self, request, *args, **kwargs)
        context['menu_list_notempty'] = get_nonempty_menu_list()
        context['previous_page'] = self.get_parent().url
        return context



